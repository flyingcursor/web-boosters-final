<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'fc_91springboard_booster' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Gd{2Zx2MOnK$U~nVc@TOZv=6jR@M}_{|?hX=uv=}T|A%}$NY}w6DLt-K#h&B+!34' );
define( 'SECURE_AUTH_KEY',  'pZ11sf|^eN,>oKD},2;}&`wsw @}}sP&-;my/|IctbG(jcUlS^yk$Qjr /7XZ`a5' );
define( 'LOGGED_IN_KEY',    '?uUJ[W%| 0UWONDWnjv]jL8#KznR`Ht_T7{t`YP}TiDh`vISefC_=/_{hY<`22~&' );
define( 'NONCE_KEY',        'bsgCyT:bK2-N6-t1Dlkv(J+zE<>l%egqr>OGjq$>UKiV`4wOX@u0zekqZBnbMJss' );
define( 'AUTH_SALT',        '_7Z% ~x6+zQw0?GM]Ps%c,=DCZa9y]Gq$I6vXx&bEPS[bVzf#yvgdL=q3U+UBv-a' );
define( 'SECURE_AUTH_SALT', '.|b}OUnj lmr^[MLLJv<lls)K|+!&w2`3cxO-?%1<23p~%15eeGq@zZ e!iC<310' );
define( 'LOGGED_IN_SALT',   '.#[VwR[T~}Yf9v^k>SPN3hu/O?1ei>?rW3GaVt#H3%_dR~T?CkL_Ev>li32;(Od4' );
define( 'NONCE_SALT',       'o*kF}:c<WC3aT((2KFw #06n,%.;c@E]f@H=V?1K~<6A_3@S~t>$[1n <RS/Q16%' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
