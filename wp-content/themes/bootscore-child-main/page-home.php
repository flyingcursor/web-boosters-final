<?php get_header(); ?>



<section class="main-section py-3">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3">
                <div class="sidebar">
                    <div id="nav-tab" class="nav nav-tabs primary-tabs" role="tablist"><button class="btn nav-categories-tab nav-link active" id="nav-categories-tab" type="button" data-bs-toggle="tab" data-bs-target="#nav-categories" role="tab" aria-controls="nav-categories" aria-selected="true">Categories</button><button class="btn nav-applications-tab nav-link" id="nav-applications-tab" type="button" data-bs-toggle="tab" data-bs-target="#nav-applications" role="tab" aria-controls="nav-applications" aria-selected="false">My Applications</button></div>
                    <div class="sidebar-content">
                        <div class="search-container"><img src="<?php echo bloginfo('stylesheet_directory'); ?> /img/search_boosters.png"><input type="text" placeholder="Search"></div>
                        <div id="nav-tabContent" class="tab-content">
                            <div id="nav-categories" class="tab-pane fade show active" role="tabpanel" aria-labelledby="nav-categories-tab">
                                <ul id="myTab" class="nav nav-tabs myTab">
                                    <?php
                                    $categories = get_categories(array(
                                        'orderby' => 'term_id',
                                        'order'   => 'ASC'
                                    ));

                                    // $categories = get_categories(); 
                                    // print_r($categories);
                                    foreach ($categories as $category) {
                                        $cat_id = $category->cat_ID;
                                        print_r($category->term_id);

                                    ?>
                                        <li class="nav-item">
                                            <a class="nav-link list-category" href="#all" data-bs-toggle="tab">
                                                <div class="checkbox-container">
                                                    <div class="form-check"><input class="form-check-input" type="checkbox" id="<?php echo $category->cat_ID; ?>"><label class="form-check-label" for="<?php echo $category->cat_ID; ?>"><?php echo $category->cat_name; ?></label></div>
                                                </div>
                                            </a>
                                        </li>
                                    <?php
                                    }
                                    ?>

                                    <!-- <li class="nav-item"><a class="nav-link list-category" href="#accounting" data-bs-toggle="tab">
                                                <div class="checkbox-container">
                                                    <div class="form-check"><input class="form-check-input" type="checkbox" id="formCheck-2"><label class="form-check-label" for="formCheck-2">Accounting</label></div>
                                                </div>
                                            </a>
                                        </li> -->
                                </ul>
                            </div>
                            <div id="nav-applications" class="tab-pane fade" role="tabpanel" aria-labelledby="nav-applications-tab">
                                <div id="nav-categories-1" class="tab-pane fade show active" role="tabpanel" aria-labelledby="nav-categories-tab">
                                    <ul id="myTab-1" class="nav nav-tabs myTab">
                                        <li class="nav-item">
                                            <a class="nav-link active list-category" href="#accounting" data-bs-toggle="tab">
                                                <div class="checkbox-container">
                                                    <div class="form-check"><input class="form-check-input" type="checkbox" id="formCheck-13"><label class="form-check-label" for="formCheck-13">All</label></div>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-9 booster-list tab-content mt-5">

                <div id="all" class="tab-pane fade active show">
                    <?php

                    $category = get_queried_object();
                    print_r($category->term_id);
                    print_r($cat_id);

                    $args = array(
                        'post_type' => 'Booster Categories',
                        'post_status' => 'publish',
                        'posts_per_page' => 6,
                        'order' => 'ASC',
                        'cat' => $cat_id,
                    );


                    $the_query = new WP_Query($args);
                    // $myposts = get_posts($args);
                    // $category = get_the_category($category_id);

                    if ($the_query->have_posts()) : ?>
                        <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                            <div class="py-2">
                                <div class="row">
                                    <div class="col-12 col-md-3 booster-image"><img class="img-fluid" src="<?php echo bloginfo('stylesheet_directory'); ?> /img/1mg-logo.png"></div>
                                    <div class="col-12 col-md-9">
                                        <p class="list-title"><?php echo get_field('short_description'); ?><br></p>
                                        <div id="collapse-content-one" class="collapse">
                                            <div class="list-description">
                                                <?php echo get_field('full_description'); ?>
                                                <div>
                                                    <button class="btn float-start" type="button" data-bs-toggle="collapse" aria-expanded="false">Apply</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div><a class="btn float-end show_hide" role="button" data-bs-toggle="collapse" aria-expanded="false" href="#collapse-content-one">Read more &amp; Apply</a></div>
                                    </div>
                                </div>
                            </div>
                    <?php
                        endwhile;
                    endif;

                    ?>
                </div>


                <div class="text-center py-3"><button class="btn" type="button">Load more</button></div>
            </div>
        </div>
    </div>
</section>




<?php get_footer(); ?>