<?php

get_header();

?>


<section class="d-flex justify-content-center align-items-center form-section">
   <div class="card">
      <div class="card-body pt-5">
         <h2 class="text-center">Login</h2>
         <?php  echo do_shortcode('[contact-form-7 id="63" title="Login"]'); ?>
         <p class="text-center">
            <a class="btn reset-pw p-1" href="#">
            RESET PASSWORD
            </a>
            |
            <a class="btn reset-pw p-1" href="#">
            LOGIN ISSUES
            </a>
         </p>
         <p class="text-center mb-0 tandc">By Logging in you are accepting our
            <br>
            <a class="btn reset-pw" href="#">
            Terms Of Service
            </a>
            &amp;
            <a class="btn reset-pw" href="#">
            Privacy Policy
            </a>
         </p>
      </div>
   </div>
</section>

<?php
get_footer();


