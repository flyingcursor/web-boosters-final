<?php
   get_header();
   ?>
<section class="d-flex justify-content-center align-items-center form-section">
   <div class="card">
      <div class="card-body pt-5">
         <div class="text-center">
            <h2>Register</h2>
         </div>
         <div class="row">
            <?php  echo do_shortcode('[contact-form-7 id="98" title="Register"]'); ?>
         </div>
         <p class="text-center">
            <a class="btn reset-pw p-1" href="#">
            RESET PASSWORD
            </a>
            |
            <a class="btn reset-pw p-1" href="#">
            EDIT PROFILE
            </a>
         </p>
         <p class="text-center mb-0 tandc">By Logging in you are accepting our
            <br>
            <a class="btn reset-pw" href="#">
            Terms Of Service
            </a>
            &amp;
            <a class="btn reset-pw" href="#">
            Privacy Policy
            </a>
         </p>
      </div>
   </div></section>

   <?php
   get_footer();
   ?>