<?php

// style and scripts
add_action('wp_enqueue_scripts', 'bootscore_child_enqueue_styles');
function bootscore_child_enqueue_styles() {

    // style.css
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');

    // Compiled Bootstrap
    $modified_bootscoreChildCss = date('YmdHi', filemtime(get_stylesheet_directory() . '/css/lib/bootstrap.min.css'));
    wp_enqueue_style('bootstrap', get_stylesheet_directory_uri() . '/css/lib/bootstrap.min.css', array('parent-style'), $modified_bootscoreChildCss);
    wp_register_style('navigation-clean', get_stylesheet_directory_uri() . '/css/Navigation-Clean.css', array('parent-style', 'bootstrap'), 1);
    wp_register_style('custom-style', get_stylesheet_directory_uri() . '/css/styles.css', array('parent-style', 'bootstrap', 'bootscore-style', 'fontawesome', 'navigation-clean'), 1.3);
    wp_enqueue_style('custom-style');

    // custom.js
    wp_enqueue_script('custom-js', get_stylesheet_directory_uri() . '/js/custom.js', false, '', true);

    wp_localize_script( 'custom-js', 'ajax_login_object', array(
        'redirecturl' => home_url()
    ));

}

/**
 * Create user and make user login.
 */
function wpm_create_user_form_registration( $cfdata ) {


    if ( ! isset( $cfdata->posted_data ) && class_exists( 'WPCF7_Submission' ) ) {
        // Contact Form 7 version 3.9 removed $cfdata->posted_data and now
        // we have to retrieve it from an API
        $submission = WPCF7_Submission::get_instance();
        if ( $submission ) {
            $formdata = $submission->get_posted_data();
        }
    } elseif ( isset( $cfdata->posted_data ) ) {
        // For pre-3.9 versions of Contact Form 7
        $formdata = $cfdata->posted_data;
    } else {
        // We can't retrieve the form data
        return $cfdata;
    }

    // Check this is the user registration form
    if ( $cfdata->title() == 'Registration' ) {
        $username = $formdata['username'];
        $email    = $formdata['email'];
        $password = $formdata['password'];
        $fullname = explode(' ', $formdata['full-name']);
        $fname    = $fullname[0];
        $lname = "";
        $mobile  = $formdata['mobile'];
        $state = $formdata['state'];
        $city   = $formdata['city'];
        $medicalregistration = $formdata['medical-registration'];
        $specialty = $formdata['specialty'];

        if(count($fullname) >= 2) {
            array_shift($fullname);
            $i = 1;
            foreach($fullname as $lastname) {
                if($i>1) {
                    $lname .= ' ';
                }
                $lname .= $lastname;
                $i++;
            }
        }

        if ( ! email_exists( $email ) ) {
            // Find an unused username
            $username_tocheck = $username;
            $i                = 1;
            while ( username_exists( $username_tocheck ) ) {
                $username_tocheck = $username . $i++;
            }
            $username = $username_tocheck;
            // Create the user
            $userdata = array(
                'user_login'   => $username,
                'user_pass'    => $password,
                'user_email'   => $email,
                'nickname'     => $fname . ' ' . $lname,
                'display_name' => $fname . ' ' . $lname,
                'first_name'   => $fname,
                'last_name'    => $lname,
                'role'         => 'subscriber'
            );

            $user_id = wp_insert_user( $userdata );

            if ( ! is_wp_error( $user_id ) ) {

                update_field('field_5e9978b0cb587', $mobile, 'user_' . $user_id);
                update_field('field_5eb5ee7390715', $state, 'user_' . $user_id);
                update_field('field_5e9978cfcb588', $city, 'user_' . $user_id);
                update_field('field_5e997932cb589', $medicalregistration, 'user_' . $user_id);
                update_field('field_5ea68d8d48de5', $specialty, 'user_' . $user_id);
                wp_set_current_user( $user_id );
                wp_set_auth_cookie( $user_id );
            }
        }
    }


    /**
     * If user logged in
     */
    // if ( $cfdata->title() == 'login' ) {

    // }

    return $cfdata;

}
add_action( 'wpcf7_before_send_mail', 'wpm_create_user_form_registration', 1 );

/**
 * login registration validation
 */
function login_registration_validation ( $cfdata, $tags ) {

    // retrieve the posted email
    if ( ! isset( $cfdata->posted_data ) && class_exists( 'WPCF7_Submission' ) ) {
        // Contact Form 7 version 3.9 removed $cfdata->posted_data and now
        // we have to retrieve it from an API
        $submission = WPCF7_Submission::get_instance();
        if ( $submission ) {
            $formdata = $submission->get_posted_data();
        }
    } elseif ( isset( $cfdata->posted_data ) ) {
        // For pre-3.9 versions of Contact Form 7
        $formdata = $cfdata->posted_data;
    } else {
        // We can't retrieve the form data
        return $cfdata;
    }

    // if already in database, invalidate for registration
    if ( $_POST['_wpcf7'] == "265" ) {
        $email = $formdata['email'];
        $mobileNo = $formdata['mobile'];
        if( email_exists( $email ) ) // email_exists is a WP function
            $cfdata->invalidate('email', 'User already Registered. Please try Login.');
        if( !preg_match('/^[6-9]\d{9}$/', $mobileNo) ) {
            $cfdata->invalidate('mobile', 'The mobile number must be valid 10 digits.');
        }
    }
    /**
     * To check user is correct (validation)
     */
    if( $_POST['_wpcf7'] == "63" ) {

        // First check the nonce, if it fails the function will break
        // check_ajax_referer( 'ajax-login-nonce', 'security' );

        if (filter_var($formdata['email'], FILTER_VALIDATE_EMAIL)) {
            $user = get_user_by('email', $formdata['email']);
        }

        $info = array();
        $info['user_login'] =  $formdata['email'];
        $info['user_password'] = $formdata['password'];
        $info['remember'] = true;

        $user_signon = wp_signon( $info, false );


        if ( is_wp_error($user_signon) ){
            $cfdata->invalidate('email', 'Wrong Email or Password');
        }


    }


    /***
     * reset password
     */
    // if( $_POST['_wpcf7'] == "279" ) {
    //     // First check the nonce, if it fails the function will break
    //     check_ajax_referer( 'ajax-login-nonce', 'security' );
    //     $random_password = $formdata['password'];
    //     $account = $formdata['email'];
    //     $user = get_user_by('email', $account );
    //     $update_user = wp_update_user( array ( 'ID' => $user->ID, 'user_pass' => $random_password ) );

    //     if ( is_wp_error($update_user) ){
    //         $cfdata->invalidate('email', 'Wrong Email or Password');
    //     }

    // }
    return $cfdata;
}
add_filter( 'wpcf7_validate', 'login_registration_validation', 10, 2 );


/**
 * Add Custom post type after registration
 */
function create_custom_posttype(){

    register_post_type( 'registrations',
        array(
            'labels' => array(
                'name' => __( 'User Registrations' ),
                'singular_name' => __( 'User Registration' )
            ),
            'menu_icon' => 'dashicons-welcome-write-blog',
            'public' => true,
            'has_archive' => false,
            'show_in_rest' => false,
            'supports' => array( 'title' ),
            'publicly_queryable' => false
        )
    );

}
add_action( 'init', 'create_custom_posttype' );

add_filter( 'if_menu_conditions', 'wpb_new_menu_conditions' );

function wpb_new_menu_conditions( $conditions ) {
    $conditions[] = array(
        'name'    =>  'If it is Custom Post Type archive', // name of the condition
        'condition' =>  function($item) {          // callback - must return TRUE or FALSE
            return is_post_type_archive();
        }
    );

    return $conditions;
}

function save_data_to_cpt($contact_form){
    $submission = WPCF7_Submission::get_instance();
    if (!$submission){
        return;
    }
    $posted_data = $submission->get_posted_data();

    if( $_POST['_wpcf7'] == "98" ) {

        if( isset($posted_data['company-name']) && !empty($posted_data['company-name']) ){
            $new_post['post_title'] = $posted_data['company-name'];
        } else {
            return;
        }

        $new_post['post_type'] = 'registrations';

        $new_post['post_status'] = 'pending';

        if($post_id = wp_insert_post($new_post)){
            update_field('company_poc', $posted_data['company-poc'] , $post_id);
            update_field('your_designation', $posted_data['your-designation'] , $post_id);
            update_field('register_email', $posted_data['regd-email'] , $post_id);
            update_field('phone_number', $posted_data['phone-number'] , $post_id);
            update_field('company_kyc', $posted_data['company-kyc'] , $post_id);

                $uploaded_files = $submission->uploaded_files();

                $upload_dir = wp_upload_dir(); // Set upload folder

                $unique_file_name = wp_unique_filename( $upload_dir['path'], $_FILES['company-kyc']['name'] ); // Generate unique name
                $filename = basename( $unique_file_name ); // Create image file name

                // Check folder permission and define file location
                if( wp_mkdir_p( $upload_dir['path'] ) ) {
                    $file = $upload_dir['path'] . '/' . $filename;
                } else {
                    $file = $upload_dir['basedir'] . '/' . $filename;
                }

                // print_r($uploaded_files['company-kyc'][0]);
                copy($uploaded_files['company-kyc'][0], $file);

                //Check image file type
                $wp_filetype = wp_check_filetype( $filename, null );

                // Set attachment data
                $attachment = array(
                    'post_mime_type' => $wp_filetype['type'],
                    'post_title'     => sanitize_file_name( $filename ),
                    'post_content'   => '',
                    'post_status'    => 'inherit'
                );

                // Create the attachment
                $attach_id = wp_insert_attachment( $attachment, $file, $post_id );

                // Include image.php
                require_once(ABSPATH . 'wp-admin/includes/image.php');

                // Define attachment metadata
                $attach_data = wp_generate_attachment_metadata( $attach_id, $file );

                // Assign metadata to attachment
                wp_update_attachment_metadata( $attach_id, $attach_data );

                // And finally assign featured image to post
                set_post_thumbnail( $post_id, $attach_id );

                $meta_value = $post_id + 1;

                update_post_meta( $post_id, 'company_kyc' , $meta_value );


        } else {
            @mail('example@example.com', 'Urgent attention needed on exampel.com share story', 'There is error in uploading share story patient images or other data. Please check.');
        }
    }
    return;
}

add_action('wpcf7_mail_sent','save_data_to_cpt');

function my_wp_nav_menu_args( $args = '' ) {

    if( is_user_logged_in() ) {
        $args['theme-location'] = 'main-menu';
        $args['menu'] = 'Logged-Out Menu';
    } else {
        $args['theme-location'] = 'main-menu';
        $args['menu'] = 'Logged-in Menu';
    }
    return $args;
}
add_filter( 'wp_nav_menu_args', 'my_wp_nav_menu_args' );


