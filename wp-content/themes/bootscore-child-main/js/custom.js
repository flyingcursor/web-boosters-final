jQuery(function ($) {

    // Do stuff here
    $(document).ready(function () {
        /**
         * header space div
         */
        var headerHeight = $('header').outerHeight(); //get header height
        $("#home-space").css("height", headerHeight);

        /**
         content  tab show
         */
        $(".myTab a").click(function (e) {
            e.preventDefault();
            $(this).tab("show");
        });


        /**
         * toggle button text on click
         */
        $(".show_hide").on("click", function () {
            //var target = $(this).attr('aria-expanded');
            var text = $(this).text();
            if (text === "Read more & Apply") {
                $(this).html('Read less');
            } else {
                $(this).html('Read more & Apply');
            }
        });

        /**
         * checkbox tick
         */

        $(".myTab li:nth-child(1) a").addClass("custom-checkbox");
        $(".myTab li a").on("click", function () {
            var check_checkbox = $(this).hasClass("custom-checkbox");
            if (check_checkbox === false) {
                $(this).addClass("custom-checkbox");
            } else {
                $(this).removeClass("custom-checkbox");
            }
        });


        /**
         * doc ready end here
         */

         document.addEventListener('wpcf7mailsent', function (event) {
            if (event.detail.contactFormId == "63" ) {
                var append = '';
                if (event.detail.contactFormId == "63" )
                    append = '/'
                window.location.replace(ajax_login_object.redirecturl + append);
            }
        }, false);

        document.addEventListener('wpcf7mailsent', function (event) {
            if (event.detail.contactFormId == "98" ) {
                var append = '';
                if (event.detail.contactFormId == "98" )
                    append = '/'
                window.location.replace(ajax_login_object.redirecturl + append);
            }
        }, false);


    });

}); // jQuery End
